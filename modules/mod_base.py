class BaseCheck:
    def __init__(self, config):
        self.proto = config["proto"]
        self.host = config["host"]
        self.args = config["args"]
        self.sections = {}

    def init_section(self, section_name):
        self.sections[section_name] = {"passed": None, "msg": None}

    def section_passed(self, section_name, passed, msg):
        self.sections[section_name]["passed"] = passed
        self.sections[section_name]["msg"] = str(msg)

    @property
    def passed(self):
        has_failed = False
        for section in self.sections.values():
            if section["passed"] is False:
                has_failed = True
        return not has_failed
