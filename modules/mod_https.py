import http.client
import OpenSSL
from datetime import datetime, timedelta

from modules.mod_base import BaseCheck

EXPIRE_MIN_DAYS = 15



class HttpsCheck(BaseCheck):
    def __init__(self, config):
        super().__init__(config)
        self.init_section("request")
        self.init_section("cert")

def check_https(checkconfig):
    result = HttpsCheck(checkconfig)
    host = checkconfig["host"]
    try:
        conn = http.client.HTTPSConnection(host)
        conn.request("GET", "/")
        resp = conn.getresponse()
    except Exception as e:
        result.section_passed("request", passed=False, msg=e)
        return result

    result.section_passed(
        "request",
        passed=resp.status < 400,
        msg=resp.status)

    try:
        certbin = conn.sock.getpeercert(binary_form=True)
        cert = OpenSSL.crypto.load_certificate(
            OpenSSL.crypto.FILETYPE_ASN1, certbin)
        after_ts = cert.get_notAfter().decode("ascii")
        expires = datetime.strptime(after_ts, "%Y%m%d%H%M%SZ")
    except Exception as e:
        result.section_passed("cert", passed=False, msg=e)
        return result

    expire_days = (expires - datetime.now()).days
    result.section_passed(
        "cert",
        passed=expire_days >= EXPIRE_MIN_DAYS,
        msg=expire_days
    )
    return result
