import socket
import time
import jinja2
import errno

from modules.mod_base import BaseCheck

FAMILY_NAMES = {
    socket.AF_INET: "ipv4",
    socket.AF_INET6: "ipv6"
}

class TcpCheck(BaseCheck):
    def __init__(self, config, port):
        super().__init__(config)
        self.init_section("ipv4")
        self.init_section("ipv6")
        self.section_passed("ipv4", None, "Unreachable")
        self.section_passed("ipv6", None, "Unreachable")
        self.port = port

def check_tcp(checkconfig):
    host = checkconfig["host"]
    port = int(checkconfig["args"][0])
    result = TcpCheck(checkconfig, port)

    try:
        ai = socket.getaddrinfo(host, port, type=socket.SOCK_STREAM)
    except socket.gaierror:
        result.section_passed("ipv4", None, "Name resolution failure")
        result.section_passed("ipv6", None, "Name resolution failure")
        return result
    for info in ai:
        famname = FAMILY_NAMES[info[0]]
        sock = socket.socket(info[0], info[1])
        sock.settimeout(3)
        start = time.monotonic()
        try:
            sock.connect(info[4])
            end = time.monotonic()
        except OSError as oserror:
            if oserror.errno == errno.ENETUNREACH:
                continue
            else:
                result.section_passed(famname, passed=False, msg=oserror)
                continue
        except Exception as e:
            result.section_passed(famname, passed=False, msg=e)
            continue
        finally:
            sock.close()
        result.section_passed(
            famname,
            passed=True,
            msg=round((end - start) * 1000))

    return result
