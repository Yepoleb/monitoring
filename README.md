# Monitoring

Custom monitoring script for my servers. Doesn't have many features but also has very few dependencies and requires no daemon on the monitored server.

# Requirements

* Python 3.5
* Jinja2
* OpenSSL for Python

# License

CC0
