import jinja2
import json
import sys
import argparse
import datetime
import getpass
import email
import smtplib

from modules.mod_tcp import check_tcp
from modules.mod_https import check_https

# config, html_path, log_path, mail_to
argparser = argparse.ArgumentParser(description="Monitor server status")
argparser.add_argument("-c", "--config", dest="config", required=True, help="Location of the config file")
argparser.add_argument("--html", dest="html", help="Path for generated HTML file")
argparser.add_argument("--log", dest="log", help="Path of logfile")
argparser.add_argument("--mail", dest="mail", help="Email to notify in case of error")
args = argparser.parse_args()

configfile = open(args.config)

servers = {}

curserver = ""
for line in configfile:
    strpline = line.strip()
    if not strpline:
        continue
    if line.startswith(" "):
        lineparts = strpline.split()
        entry = {
            "proto": lineparts[0],
            "host": lineparts[1],
            "args": lineparts[2:]
        }
        servers[curserver].append(entry)
    else:
        curserver = strpline
        servers[curserver] = []

timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
results = {}
has_failed = False
for server, checks in servers.items():
    print("Server", server)
    results[server] = []
    for check in checks:
        print("Check", check["proto"], check["host"], *check["args"])
        checkname = "check_" + check["proto"]
        checkfunc = globals()[checkname]
        result = checkfunc(check)
        if not result.passed:
            print("FAILED")
            has_failed = True
        results[server].append(result)
    print()

if args.html:
    html_env = jinja2.Environment(
        loader=jinja2.FileSystemLoader("templates"),
        autoescape=True, trim_blocks=True, lstrip_blocks=True,
        keep_trailing_newline=False)
    html_template = html_env.get_template("web_templ.html")
    html_render = html_template.render(results=results, timestamp=timestamp)
    with open(args.html, "w") as html_file:
        html_file.write(html_render)

txt_env = jinja2.Environment(
    loader=jinja2.FileSystemLoader("templates"),
    autoescape=False, trim_blocks=True, lstrip_blocks=True,
    keep_trailing_newline=False)
txt_template = txt_env.get_template("report_templ.txt")
txt_render = txt_template.render(results=results, timestamp=timestamp).strip() + "\n"

if args.log:
    with open(args.log, "a") as log_file:
        log_file.write(txt_render)
        log_file.write("\n\n\n")

if args.mail and has_failed:
    msg = email.message.EmailMessage()
    msg.set_content(txt_render)

    msg["Subject"] = "Server status notification {}".format(timestamp)
    msg["From"] = getpass.getuser()
    msg["To"] = args.mail

    smtp_sess = smtplib.SMTP("localhost")
    smtp_sess.send_message(msg)
    smtp_sess.quit()
